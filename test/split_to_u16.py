#top = lib::split_to_u8_th

from cocotb.triggers import Timer
from cocotb.clock import Clock
from spade import FallingEdge, SpadeExt
from cocotb import cocotb

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    # To access unmangled signals as cocotb values (without the spade wrapping) use
    # <signal_name>_i
    # For cocotb functions like the clock generator, we need a cocotb value
    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.ready = False;
    s.i.value = "None";

    s.i.rst = True
    await FallingEdge(clk);
    s.i.rst = False

    s.o.assert_eq("None")
    await FallingEdge(clk);
    s.o.assert_eq("None")

    s.i.value = "Some(0xABCD)"
    await FallingEdge(clk);
    s.i.value = "None"
    # Until we assert ready, nothing changes
    s.o.assert_eq("Some(0xCD)")
    await FallingEdge(clk);
    s.o.assert_eq("Some(0xCD)")
    await FallingEdge(clk);
    s.o.assert_eq("Some(0xCD)")
    s.i.ready = True
    await FallingEdge(clk);
    s.o.assert_eq("Some(0xAB)")
    await FallingEdge(clk);
    s.o.assert_eq("None")

    s.i.value = "Some(0x1234)"
    await Timer(1, units = 'ns')
    s.o.assert_eq("Some(0x34)")
    await FallingEdge(clk);
    s.o.assert_eq("Some(0x12)")
    await FallingEdge(clk)
    s.o.assert_eq("Some(0x34)")
    s.i.value = "None"
    await FallingEdge(clk)
    s.o.assert_eq("Some(0x12)")
    await FallingEdge(clk)
    s.o.assert_eq("None")



